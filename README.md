# 中国 GeoJson 数据

2020年12月中华人民共和国县以上行政区

## 版本

+ 数据版本 2020年12月
+ 更新时间 2022年02月

## 使用

```bash
git clone https://gitee.com/huanggefan/chinaGeoJson.git
cd chinaGeoJson/data
```

## 更新

```bash
make all
```

## 数据展示

1. https://cdn.huanggefan.cn/geojson/demo.html

2. https://cdn.huanggefan.cn/geojson/demo-full.html

## 数据详情

数据目录：chinaGeoJson/data

+ 一级行政区数据目录：chinaGeoJson/data/province
+ 地级行政区数据目录：chinaGeoJson/data/city
+ 县级行政区数据目录：chinaGeoJson/data/county

+ 包含一级行政区： 34
+ 包含地级行政区： 333
+ 包含县级行政区： 2844

+ china.json 仅包含省份信息，详见**数据展示1**
+ china-full.json 已合并所有地级行政区，详见**数据展示2**

## 数据来源

[中华人民共和国行政区划代码](http://www.mca.gov.cn/article/sj/xzqh)

[2020年12月中华人民共和国县以上行政区划代码](http://www.mca.gov.cn/article/sj/xzqh/2020/20201201.html)

[GeoJson来源-阿里云](http://datav.aliyun.com/tools/atlas/)


## 提示

**请下载本仓库，使用离线数据**

**请不要直接引用 https://cdn.huanggefan.cn/geojson 上的数据**

**此处的数据仅用做该仓库的展示，不保证此处数据一直存在！！！**

## 注意

获取部分行政区数据时，小部分未解析出行政区划代码，小部分发生404错误，因此行政区数据不完整。

以下是未解析出行政区划代码的行政区

```
西沙区
南沙区
```

以下是发生404的行政区划代码

```
Error: ('540600', 'https://geo.datav.aliyun.com/areas/bound/540600_full.json')
Error: ('110118', 'https://geo.datav.aliyun.com/areas/bound/110118.json')
Error: ('110119', 'https://geo.datav.aliyun.com/areas/bound/110119.json')
Error: ('120119', 'https://geo.datav.aliyun.com/areas/bound/120119.json')
Error: ('130284', 'https://geo.datav.aliyun.com/areas/bound/130284.json')
Error: ('130407', 'https://geo.datav.aliyun.com/areas/bound/130407.json')
Error: ('130408', 'https://geo.datav.aliyun.com/areas/bound/130408.json')
Error: ('130505', 'https://geo.datav.aliyun.com/areas/bound/130505.json')
Error: ('130506', 'https://geo.datav.aliyun.com/areas/bound/130506.json')
Error: ('130708', 'https://geo.datav.aliyun.com/areas/bound/130708.json')
Error: ('130709', 'https://geo.datav.aliyun.com/areas/bound/130709.json')
Error: ('130881', 'https://geo.datav.aliyun.com/areas/bound/130881.json')
Error: ('131103', 'https://geo.datav.aliyun.com/areas/bound/131103.json')
Error: ('140213', 'https://geo.datav.aliyun.com/areas/bound/140213.json')
Error: ('140214', 'https://geo.datav.aliyun.com/areas/bound/140214.json')
Error: ('140215', 'https://geo.datav.aliyun.com/areas/bound/140215.json')
Error: ('140403', 'https://geo.datav.aliyun.com/areas/bound/140403.json')
Error: ('140404', 'https://geo.datav.aliyun.com/areas/bound/140404.json')
Error: ('140405', 'https://geo.datav.aliyun.com/areas/bound/140405.json')
Error: ('140406', 'https://geo.datav.aliyun.com/areas/bound/140406.json')
Error: ('140681', 'https://geo.datav.aliyun.com/areas/bound/140681.json')
Error: ('140703', 'https://geo.datav.aliyun.com/areas/bound/140703.json')
Error: ('150603', 'https://geo.datav.aliyun.com/areas/bound/150603.json')
Error: ('210115', 'https://geo.datav.aliyun.com/areas/bound/210115.json')
Error: ('210214', 'https://geo.datav.aliyun.com/areas/bound/210214.json')
Error: ('211104', 'https://geo.datav.aliyun.com/areas/bound/211104.json')
Error: ('220184', 'https://geo.datav.aliyun.com/areas/bound/220184.json')
Error: ('230717', 'https://geo.datav.aliyun.com/areas/bound/230717.json')
Error: ('230718', 'https://geo.datav.aliyun.com/areas/bound/230718.json')
Error: ('230719', 'https://geo.datav.aliyun.com/areas/bound/230719.json')
Error: ('230723', 'https://geo.datav.aliyun.com/areas/bound/230723.json')
Error: ('230724', 'https://geo.datav.aliyun.com/areas/bound/230724.json')
Error: ('230725', 'https://geo.datav.aliyun.com/areas/bound/230725.json')
Error: ('230726', 'https://geo.datav.aliyun.com/areas/bound/230726.json')
Error: ('230751', 'https://geo.datav.aliyun.com/areas/bound/230751.json')
Error: ('230883', 'https://geo.datav.aliyun.com/areas/bound/230883.json')
Error: ('231086', 'https://geo.datav.aliyun.com/areas/bound/231086.json')
Error: ('231183', 'https://geo.datav.aliyun.com/areas/bound/231183.json')
Error: ('310151', 'https://geo.datav.aliyun.com/areas/bound/310151.json')
Error: ('320213', 'https://geo.datav.aliyun.com/areas/bound/320213.json')
Error: ('320214', 'https://geo.datav.aliyun.com/areas/bound/320214.json')
Error: ('320613', 'https://geo.datav.aliyun.com/areas/bound/320613.json')
Error: ('320614', 'https://geo.datav.aliyun.com/areas/bound/320614.json')
Error: ('320685', 'https://geo.datav.aliyun.com/areas/bound/320685.json')
Error: ('320812', 'https://geo.datav.aliyun.com/areas/bound/320812.json')
Error: ('320813', 'https://geo.datav.aliyun.com/areas/bound/320813.json')
Error: ('330112', 'https://geo.datav.aliyun.com/areas/bound/330112.json')
Error: ('330213', 'https://geo.datav.aliyun.com/areas/bound/330213.json')
Error: ('330383', 'https://geo.datav.aliyun.com/areas/bound/330383.json')
Error: ('331083', 'https://geo.datav.aliyun.com/areas/bound/331083.json')
Error: ('340209', 'https://geo.datav.aliyun.com/areas/bound/340209.json')
Error: ('340210', 'https://geo.datav.aliyun.com/areas/bound/340210.json')
Error: ('340212', 'https://geo.datav.aliyun.com/areas/bound/340212.json')
Error: ('340281', 'https://geo.datav.aliyun.com/areas/bound/340281.json')
Error: ('340422', 'https://geo.datav.aliyun.com/areas/bound/340422.json')
Error: ('340705', 'https://geo.datav.aliyun.com/areas/bound/340705.json')
Error: ('340706', 'https://geo.datav.aliyun.com/areas/bound/340706.json')
Error: ('340722', 'https://geo.datav.aliyun.com/areas/bound/340722.json')
Error: ('340882', 'https://geo.datav.aliyun.com/areas/bound/340882.json')
Error: ('341504', 'https://geo.datav.aliyun.com/areas/bound/341504.json')
Error: ('341882', 'https://geo.datav.aliyun.com/areas/bound/341882.json')
Error: ('350112', 'https://geo.datav.aliyun.com/areas/bound/350112.json')
Error: ('360113', 'https://geo.datav.aliyun.com/areas/bound/360113.json')
Error: ('360404', 'https://geo.datav.aliyun.com/areas/bound/360404.json')
Error: ('360483', 'https://geo.datav.aliyun.com/areas/bound/360483.json')
Error: ('360603', 'https://geo.datav.aliyun.com/areas/bound/360603.json')
Error: ('360704', 'https://geo.datav.aliyun.com/areas/bound/360704.json')
Error: ('360783', 'https://geo.datav.aliyun.com/areas/bound/360783.json')
Error: ('361003', 'https://geo.datav.aliyun.com/areas/bound/361003.json')
Error: ('361104', 'https://geo.datav.aliyun.com/areas/bound/361104.json')
Error: ('370114', 'https://geo.datav.aliyun.com/areas/bound/370114.json')
Error: ('370115', 'https://geo.datav.aliyun.com/areas/bound/370115.json')
Error: ('370116', 'https://geo.datav.aliyun.com/areas/bound/370116.json')
Error: ('370117', 'https://geo.datav.aliyun.com/areas/bound/370117.json')
Error: ('370215', 'https://geo.datav.aliyun.com/areas/bound/370215.json')
Error: ('370505', 'https://geo.datav.aliyun.com/areas/bound/370505.json')
Error: ('370614', 'https://geo.datav.aliyun.com/areas/bound/370614.json')
Error: ('371503', 'https://geo.datav.aliyun.com/areas/bound/371503.json')
Error: ('371681', 'https://geo.datav.aliyun.com/areas/bound/371681.json')
Error: ('371703', 'https://geo.datav.aliyun.com/areas/bound/371703.json')
Error: ('410104', 'https://geo.datav.aliyun.com/areas/bound/410104.json')
Error: ('410783', 'https://geo.datav.aliyun.com/areas/bound/410783.json')
Error: ('411003', 'https://geo.datav.aliyun.com/areas/bound/411003.json')
Error: ('411603', 'https://geo.datav.aliyun.com/areas/bound/411603.json')
Error: ('420882', 'https://geo.datav.aliyun.com/areas/bound/420882.json')
Error: ('421088', 'https://geo.datav.aliyun.com/areas/bound/421088.json')
Error: ('430182', 'https://geo.datav.aliyun.com/areas/bound/430182.json')
Error: ('430212', 'https://geo.datav.aliyun.com/areas/bound/430212.json')
Error: ('430582', 'https://geo.datav.aliyun.com/areas/bound/430582.json')
Error: ('440309', 'https://geo.datav.aliyun.com/areas/bound/440309.json')
Error: ('440310', 'https://geo.datav.aliyun.com/areas/bound/440310.json')
Error: ('440311', 'https://geo.datav.aliyun.com/areas/bound/440311.json')
Error: ('450206', 'https://geo.datav.aliyun.com/areas/bound/450206.json')
Error: ('450381', 'https://geo.datav.aliyun.com/areas/bound/450381.json')
Error: ('451003', 'https://geo.datav.aliyun.com/areas/bound/451003.json')
Error: ('451082', 'https://geo.datav.aliyun.com/areas/bound/451082.json')
Error: ('451103', 'https://geo.datav.aliyun.com/areas/bound/451103.json')
Error: ('451203', 'https://geo.datav.aliyun.com/areas/bound/451203.json')
Error: ('500154', 'https://geo.datav.aliyun.com/areas/bound/500154.json')
Error: ('500155', 'https://geo.datav.aliyun.com/areas/bound/500155.json')
Error: ('500156', 'https://geo.datav.aliyun.com/areas/bound/500156.json')
Error: ('510116', 'https://geo.datav.aliyun.com/areas/bound/510116.json')
Error: ('510117', 'https://geo.datav.aliyun.com/areas/bound/510117.json')
Error: ('510118', 'https://geo.datav.aliyun.com/areas/bound/510118.json')
Error: ('510185', 'https://geo.datav.aliyun.com/areas/bound/510185.json')
Error: ('510604', 'https://geo.datav.aliyun.com/areas/bound/510604.json')
Error: ('510705', 'https://geo.datav.aliyun.com/areas/bound/510705.json')
Error: ('510981', 'https://geo.datav.aliyun.com/areas/bound/510981.json')
Error: ('511083', 'https://geo.datav.aliyun.com/areas/bound/511083.json')
Error: ('511504', 'https://geo.datav.aliyun.com/areas/bound/511504.json')
Error: ('513201', 'https://geo.datav.aliyun.com/areas/bound/513201.json')
Error: ('520204', 'https://geo.datav.aliyun.com/areas/bound/520204.json')
Error: ('520281', 'https://geo.datav.aliyun.com/areas/bound/520281.json')
Error: ('520304', 'https://geo.datav.aliyun.com/areas/bound/520304.json')
Error: ('522302', 'https://geo.datav.aliyun.com/areas/bound/522302.json')
Error: ('530115', 'https://geo.datav.aliyun.com/areas/bound/530115.json')
Error: ('530303', 'https://geo.datav.aliyun.com/areas/bound/530303.json')
Error: ('530304', 'https://geo.datav.aliyun.com/areas/bound/530304.json')
Error: ('530403', 'https://geo.datav.aliyun.com/areas/bound/530403.json')
Error: ('530481', 'https://geo.datav.aliyun.com/areas/bound/530481.json')
Error: ('530681', 'https://geo.datav.aliyun.com/areas/bound/530681.json')
Error: ('533301', 'https://geo.datav.aliyun.com/areas/bound/533301.json')
Error: ('540103', 'https://geo.datav.aliyun.com/areas/bound/540103.json')
Error: ('540104', 'https://geo.datav.aliyun.com/areas/bound/540104.json')
Error: ('540502', 'https://geo.datav.aliyun.com/areas/bound/540502.json')
Error: ('540521', 'https://geo.datav.aliyun.com/areas/bound/540521.json')
Error: ('540522', 'https://geo.datav.aliyun.com/areas/bound/540522.json')
Error: ('540523', 'https://geo.datav.aliyun.com/areas/bound/540523.json')
Error: ('540524', 'https://geo.datav.aliyun.com/areas/bound/540524.json')
Error: ('540525', 'https://geo.datav.aliyun.com/areas/bound/540525.json')
Error: ('540526', 'https://geo.datav.aliyun.com/areas/bound/540526.json')
Error: ('540527', 'https://geo.datav.aliyun.com/areas/bound/540527.json')
Error: ('540528', 'https://geo.datav.aliyun.com/areas/bound/540528.json')
Error: ('540529', 'https://geo.datav.aliyun.com/areas/bound/540529.json')
Error: ('540530', 'https://geo.datav.aliyun.com/areas/bound/540530.json')
Error: ('540531', 'https://geo.datav.aliyun.com/areas/bound/540531.json')
Error: ('540602', 'https://geo.datav.aliyun.com/areas/bound/540602.json')
Error: ('540621', 'https://geo.datav.aliyun.com/areas/bound/540621.json')
Error: ('540622', 'https://geo.datav.aliyun.com/areas/bound/540622.json')
Error: ('540623', 'https://geo.datav.aliyun.com/areas/bound/540623.json')
Error: ('540624', 'https://geo.datav.aliyun.com/areas/bound/540624.json')
Error: ('540625', 'https://geo.datav.aliyun.com/areas/bound/540625.json')
Error: ('540626', 'https://geo.datav.aliyun.com/areas/bound/540626.json')
Error: ('540627', 'https://geo.datav.aliyun.com/areas/bound/540627.json')
Error: ('540628', 'https://geo.datav.aliyun.com/areas/bound/540628.json')
Error: ('540629', 'https://geo.datav.aliyun.com/areas/bound/540629.json')
Error: ('540630', 'https://geo.datav.aliyun.com/areas/bound/540630.json')
Error: ('610118', 'https://geo.datav.aliyun.com/areas/bound/610118.json')
Error: ('610482', 'https://geo.datav.aliyun.com/areas/bound/610482.json')
Error: ('610503', 'https://geo.datav.aliyun.com/areas/bound/610503.json')
Error: ('610603', 'https://geo.datav.aliyun.com/areas/bound/610603.json')
Error: ('610681', 'https://geo.datav.aliyun.com/areas/bound/610681.json')
Error: ('610703', 'https://geo.datav.aliyun.com/areas/bound/610703.json')
Error: ('610803', 'https://geo.datav.aliyun.com/areas/bound/610803.json')
Error: ('610881', 'https://geo.datav.aliyun.com/areas/bound/610881.json')
Error: ('620881', 'https://geo.datav.aliyun.com/areas/bound/620881.json')
Error: ('630106', 'https://geo.datav.aliyun.com/areas/bound/630106.json')
Error: ('632301', 'https://geo.datav.aliyun.com/areas/bound/632301.json')
Error: ('632803', 'https://geo.datav.aliyun.com/areas/bound/632803.json')
Error: ('650502', 'https://geo.datav.aliyun.com/areas/bound/650502.json')
Error: ('650521', 'https://geo.datav.aliyun.com/areas/bound/650521.json')
Error: ('650522', 'https://geo.datav.aliyun.com/areas/bound/650522.json')
Error: ('652902', 'https://geo.datav.aliyun.com/areas/bound/652902.json')
Error: ('659009', 'https://geo.datav.aliyun.com/areas/bound/659009.json')
Error: ('659010', 'https://geo.datav.aliyun.com/areas/bound/659010.json')
```

## 免责

+ 本项目仅提供了获取中国所有行政区的GeoJson数据的思路。
+ 本项目包含的数据，仅用于展示，数据的使用与本项目无关。
+ 本项目不对使用本项目代码获取到的任何数据做任何准确性的保证。
